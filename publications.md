---
layout: page
title: Publications
permalink: /publications/
nav: true
---

Talks, Videos & Others
---

{:.year}
### 2020

{:.paper}
<span>Connect with Experts: Using CUDA, TensorRT and DriveWorks on NVIDIA DRIVE AGX</span>{:.papertitle}  
<span>_NVIDIA_, 2020</span>{:.journal}  
<span>Connect With Expert Session</span>{:.comment}  
<span>**Link:** [https://developer.nvidia.com/gtc/2020/video/cwe21185-vid](https://developer.nvidia.com/gtc/2020/video/cwe21185-vid)</span>{:.doi}

{:.paper}
<span>Connect With Experts: Jumpstart AV Development with DRIVE OS and DriveWorks</span>{:.papertitle}  
<span>_NVIDIA_, 2020</span>{:.journal}  
<span>Connect With Expert Session</span>{:.comment}  
<span>**Link:** [https://developer.nvidia.com/gtc/2020/video/cwe21184-vid](https://developer.nvidia.com/gtc/2020/video/cwe21184-vid)</span>{:.doi}


{:.year}
### 2019

{:.paper}
<span>DRIVE Labs: Take a Ride in NVIDIA's Self-Driving Car</span>{:.papertitle}  
<span>_NVIDIA_, 2019</span>{:.journal}  
<span>Drive Labs Video</span>{:.comment}  
<span>**Link:** [https://youtu.be/1W9q5SjaJTc](https://youtu.be/1W9q5SjaJTc)</span>{:.doi}

{:.year}
### 2017

{:.paper}
<span>Deep Neural Networks: Changing the Autonomous Vehicle Landscape</span>{:.papertitle}  
<span>W.L.D. Lui</span>{:.authors}  
<span>_Hotchips_, 2017</span>{:.journal}  
<span>Tutorial/Talk</span>{:.comment}  
<span>**Link:** [https://youtu.be/4zrat3Mn410](https://youtu.be/4zrat3Mn410)</span>{:.doi}

{:.paper}
<span>DriveWorks: A Look Inside NVIDIA's Autonomous Driving SDK</span>{:.papertitle}  
<span>M. Sainz, G. Agarwal and W.L.D. Lui</span>{:.authors}  
<span>_NVIDIA GTC Conference_, 2017</span>{:.journal}  
<span>Talk</span>{:.comment}  
<span>**Link:** [http://on-demand.gputechconf.com/gtc/2017/video/s7427-driveworks-a-look-inside-nvidias-autonomous-driving-skK_1.mp4](http://on-demand.gputechconf.com/gtc/2017/video/s7427-driveworks-a-look-inside-nvidias-autonomous-driving-skK_1.mp4)</span>{:.doi}

{:.year}
### 2006
{:.paper}
<span>Vehicle’s License Plate OCR (Optical Character Recognition) System</span>{:.papertitle}  
<span>W. L. D. Lui, B. H. Khoo, and A. K. B. See</span>{:.authors}  
<span>_National Instruments_, 2006</span>{:.journal}  
<span>ASEAN Customer Solution Whitepaper</span>{:.comment}  

Journal Articles
---

{:.year}
### 2012

{:.paper}
<span>A Pure Vision-based Topological SLAM System</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_The International Journal of Robotics Research_, vol. 31, iss. 4, pp. 403-428, 2012</span>{:.journal}  
<span>**Link:** [http://ijr.sagepub.com/content/31/4/403](http://ijr.sagepub.com/content/31/4/403)</span>{:.doi}

{:.year}
### 2010

{:.paper}
<span>Eye-Full Tower: A GPU-based variable multibaseline omnidirectional stereovision system with automatic baseline selection for outdoor mobile robot navigation</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Robotics and Autonomous Systems_, vol. 58, iss. 6, pp. 747-761, 2010</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1016/j.robot.2010.02.007](http://dx.doi.org/10.1016/j.robot.2010.02.007)</span>{:.doi}

{:.year}
### 2008

{:.paper}
<span>A Malaysian icense late localization and recognition system</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Journal of Systemics, Cybernetics and Informatics_, vol. 6, iss. 1, 2008</span>{:.journal}  

{:.year}
### 2007

{:.paper}
<span>A license plate segmentation model for Malaysian cars</span>{:.papertitle}  
<span>A. S. K. Bin, W. L. D. Lui, and Y. Shi</span>{:.authors}  
<span>_International Journal of Innovative Computing, Information and Control_, vol. 3, pp. 1565-1582, 2007.</span>{:.journal}  

Conference Publications and Posters
---

{:.year}
### 2013

{:.paper}
<span>Going beyond vision to improve bionic vision</span>{:.papertitle}  
<span>W. H. Li, T. J. J. Tang, and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Image Processing_, Melbourne, Australia, 2013, pp. 1555-1558</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ICIP.2013.6738320](http://dx.doi.org/10.1109/ICIP.2013.6738320)</span>{:.doi} 

{:.paper}
<span>sychophysics testing bionic vision image processing algorithms using an FPGA hatpack</span>{:.papertitle}  
<span>H. Josh, C. Mann, L. Kleeman, and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Image Processing_, Melbourne, Australia, 2013, pp. 1550-1554.</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ICIP.2013.6738319](http://dx.doi.org/10.1109/ICIP.2013.6738319)</span>{:.doi} 

{:.year}
### 2012

{:.paper}
<span>Transformative Reality: Improving bionic vision with robotic sensing</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the International Conference of the IEEE Engineering in Medicine and Biology Society_, CA, USA, 2012, pp. 304-307</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/EMBC.2012.6345929](http://dx.doi.org/10.1109/EMBC.2012.6345929)</span>{:.doi}

{:.paper}
<span>Robust egomotion estimation using ICP in inverse depth coordinates</span>{:.papertitle}  
<span>W. L. D. Lui, T. J. J. Tang, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Robotics and Automation_, MN, USA, 2012, pp. 1671-1678</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ICRA.2012.6225354](http://dx.doi.org/10.1109/ICRA.2012.6225354)</span>{:.doi} 

{:.paper}
<span>Plane-based detection of staircases using inverse depth</span>{:.papertitle}  
<span>T. J. J. Tang, W. L. D. Lui, and W. H. Li</span>{:.authors}  
<span>Australasian Conference on Robotics and Automation, Wellington, New Zealand, 2012</span>{:.journal}

{:.year}
### 2011

{:.paper}
<span>Transformative Reality: Augmented reality for visual prostheses</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the IEEE International Symposium on Mixed and Augmented Reality_, Basel, Switzerland, 2011, pp. 253-254</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ISMAR.2011.6092402](http://dx.doi.org/10.1109/ISMAR.2011.6092402)</span>{:.doi} 

{:.paper}
<span>Transformative Reality: Intelligent Algorithms to Improve Low Resolution Bionic Vision</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>Poster</span>{:.comment}  
<span>_International Conference on Medical Bionics_, Melbourne, Australia, 2011</span>{:.journal}  

{:.paper}
<span>A lightweight approach to 6-DOF plane-based egomotion estimation using inverse depth</span>{:.papertitle}  
<span>T. J. J. Tang, W. L. D. Lui, and W. H. Li</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Melbourne, Australia, 2011</span>{:.journal}

{:.paper}
<span>eBug – An open robotics platform for teaching and research</span>{:.papertitle}  
<span>N. D’Ademo, W. L. D. Lui, W. H. Li, A. Sekercioglu, and T. Drummond</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Melbourne, Australia, 2011</span>{:.journal} 

{:.year}
### 2010

{:.paper}
<span>Robust Online Map Merging System using Laser Scan Matching and Omnidirectional Vision</span>{:.papertitle}  
<span>F. Tungadi, W. L. D. Lui, L. Kleeman, and R. Jarvis</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Intelligent Robots and Systems_, Taipei, Taiwan, 2010, pp. 7-14</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/IROS.2010.5654446](http://dx.doi.org/10.1109/IROS.2010.5654446)</span>{:.doi}

{:.paper}
<span>A Pure Vision-based Approach to Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Intelligent Robots and Systems_, Taipei, Taiwan, 2010, pp. 3784-3791</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/IROS.2010.5651115](http://dx.doi.org/10.1109/IROS.2010.5651115)</span>{:.doi}

{:.paper}
<span>An Active Visual Loop Closure Detection and Validation System for Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Brisbane, Australia, 2010</span>{:.journal}  

{:.paper}
<span>Face processing by honeybees; how does brightness inversion affect the capacity of the miniature brain to bind and configure spatial elements for reliable recognition?</span>{:.papertitle}  
<span>A. G. Dyer, W. H. Li, A. Schmidt, and W. L. D. Lui</span>{:.authors}  
<span>_International Conference on Cognitive Science_, Beijing, China, 2010</span>{:.journal} 

{:.year}
### 2009

{:.paper}
<span>Utilization of Webots and the Khepera II as a Platform for Neural Q-Learning Controllers</span>{:.papertitle}  
<span>V. Ganapathy, C. Y. Soh, and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the IEEE Symposium on Industrial Electronics and Applications_, Kuala Lumpur, Malaysia, 2009, pp. 783-788</span>{:.journal}  
<span>**Link:** [http://dx.doi.org/10.1109/ISIEA.2009.5356361](http://dx.doi.org/10.1109/ISIEA.2009.5356361)</span>{:.doi}

{:.year}
### 2008

{:.paper}
<span>An Omnidirectional Vision System for Outdoor Mobile Robots</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_SIMPAR Workshop on Omnidirectional Robot Vision_, Venice, Italy, 2008, pp. 273-284</span>{:.journal}  

{:.paper}
<span>Design and Implementation of a Simulated Robot using Webots Software</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Control, Instrumentation and Mechatronics Engineering_, Johor Bahru, Malaysia, 2008, pp. 850-856</span>{:.journal}  

{:.paper}
<span>Application of Neural Q-Learning on the Khepera-II via Webots Software</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Fascinating Advancement in Mechanical Engineering_, Tamil Nadu, India, 2008</span>{:.journal}  

{:.paper}
<span>A Malaysian vehicle license plate localization and recognition system</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_World Mulitconference on Systemics, Cybernetics and Informatics_, Orlando, USA, 2007, pp. 273-284</span>{:.journal}  

{:.year}
### 2005

{:.paper}
<span>Musical Notes and Instrument Recognition using Artificial Neural Networks</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Intelligent Systems_, Kuala Lumpur, Malaysia, 2005</span>{:.journal}  

{:.paper}
<span>A Six Degree of Freedom Robot Arm Simulator using Java 3D API</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Intelligent Systems_, Kuala Lumpur, Malaysia, 2005</span>{:.journal}  

Patents
---

{:.year}
### 2012

{:.paper}
<span>System and Method for Processing Sensor Data for the Visually Impaired</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>iss. PCT/AU2012/001006, 2012</span>{:.journal}

Ph.D. Dissertation
---

{:.paper}
<span>Autonomous Robot Navigation: Appearance based Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui</span>{:.authors}  
<span>PhD Thesis, Monash University, Australia, 2011</span>{:.journal}  
<span>**Link:** [http://arrow.monash.edu.au/hdl/1959.1/473850](http://arrow.monash.edu.au/hdl/1959.1/473850)</span>{:.doi}

Undergraduate Honours Thesis
---

{:.paper}
<span>Design and Implementation of the Khepera II using Webots Software</span>{:.papertitle}  
<span>W. L. D. Lui</span>{:.authors}  
<span>_Monash University_, 2006</span>{:.journal}  

<!--
{:.year}
### 2014

{:.paper}
<span>The worst sources of name generation</span>{:.papertitle}  
<span>F.A. Author</span>{:.authors}  
<span>College, Aug. 2014</span>{:.journal}  
<span>Files at the following link</span>{:.comment}  
-->