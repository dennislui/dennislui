---
layout:		post
title:		"GTC 2017"
date:		2017-10-02 15:32:14 0800
image: 		gtc-2017-1.png
categories:	[av]
tag:		posts
---

Gave a talk about DriveWorks at GTC 2017 to about 500+ attendees!

[Link to Video](http://on-demand.gputechconf.com/gtc/2017/video/s7427-driveworks-a-look-inside-nvidias-autonomous-driving-skK_1.mp4)

![gtc2017-2]({{"/images/gtc-2017-2.png" | prepend: site.baseurl }}){:height="200px"}
![gtc2017-1]({{"/images/gtc-2017-1.png" | prepend: site.baseurl }}){:height="200px"}
