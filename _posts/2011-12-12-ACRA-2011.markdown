---
layout:		post
title:		"ACRA 2011"
date:		2011-12-12 15:32:14 0800
image: 		ACRA-2011-1.jpg
categories:	[robotics]
tag:		posts
---

Some photos taken during the Australasian Conference on Robotics and Automation held at Monash University.

![ACRA2011-1]({{"/images/ACRA-2011-1.jpg" | prepend: site.baseurl }}){:width="350px"}
![ACRA2011-4]({{"/images/ACRA-2011-4.jpg" | prepend: site.baseurl }}){:width="350px"}
![ACRA2011-2]({{"/images/ACRA-2011-2.jpg" | prepend: site.baseurl }}){:width="350px"}
![ACRA2011-3]({{"/images/ACRA-2011-3.jpg" | prepend: site.baseurl }}){:width="350px"}

