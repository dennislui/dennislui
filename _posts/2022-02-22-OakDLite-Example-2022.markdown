---
layout:     post
title:      "Oak-D-Lite Example"
date:       2022-02-22 21:57:00 0800
image:      oak-d-lite_example.png
youtubeId1: BpMys1ZS8Ag
categories: [robotics]
tag:        posts
---

It was fairly simple to get the sensor up and running on an Ubuntu 20.04 machine:

```code
sudo curl -fL https://docs.luxonis.com/install_dependencies.sh | bash
git clone https://github.com/luxonis/depthai.git
cd depthai
python3 install_requirements.py
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="03e7", MODE="0666"' | sudo tee /etc/udev/rules.d/80-movidius.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
python3 depthai_demo.py
```

{% include youtubePlayer.html id=page.youtubeId1 %}

However, it is unfortunate that the official docker container provided [here](https://hub.docker.com/r/luxonis/depthai-library) does not work as-is

```code
docker pull luxonis/depthai-library
sudo docker run --rm \
    --privileged \
    -v /dev/bus/usb:/dev/bus/usb \
    --device-cgroup-rule='c 189:* rmw' \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    luxonis/depthai-library:latest \
    python3 /depthai-python/examples/01_rgb_preview.py
```

Tried experimenting with my own containers but not much luck so far. One path is to rebuild libusb without udev in the container, which is similar to what the official container is doing as per this [Dockerfile](https://github.com/luxonis/depthai-python/blob/main/ci/Dockerfile) and another approach is based on [this](https://github.com/luxonis/depthai-docker/blob/master/Dockerfile-depthai). Will continue to experiment.

**Update (Feb 23, 2022):**

Latest stable release of the image works as per the following instructions

```code
docker pull luxonis/depthai-library:v2.14.0.0
docker run --rm \
    --privileged \
    -v /dev/bus/usb:/dev/bus/usb \
    --device-cgroup-rule='c 189:* rmw' \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    luxonis/depthai-library:v2.14.0.0 \
    python3 /depthai-python/examples/ColorCamera/rgb_preview.py
```

**Update (March 04, 2022):**

Was trying to build my own docker container for DepthAI SDK but found that the [nvidia/cudagl](https://hub.docker.com/r/nvidia/cudagl) base images do not work but the [nvidia/cuda](https://hub.docker.com/r/nvidia/cuda) base images work. After some experimentations, it was found that it was the libusb that came with the nvidia/cudagl base images that were causing the issue (as mentioned before, we have to rebuild libusb without udev in the container). Therefore, providing the Dockerfile here and some instructions if you would like to do the same.


```code
git clone https://github.com/luxonis/depthai-python.git
cd depthai-python
git submodule update --init --recursive

```

Create a DockerFile (DockerFile_CUDAGL) at `/depthai-python/ci` with the following contents

```code
FROM nvidia/cudagl:11.1.1-devel-ubuntu20.04

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/Los_Angeles

RUN apt-get update && apt-get install -y wget build-essential cmake pkg-config libjpeg-dev libtiff5-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libgtk2.0-dev libgtk-3-dev libatlas-base-dev gfortran git python3-pip usbutils

ADD ci/docker_dependencies.sh .
RUN ./docker_dependencies.sh
RUN apt-get remove -y libusb-1.0-0
RUN wget https://github.com/libusb/libusb/releases/download/v1.0.24/libusb-1.0.24.tar.bz2
RUN tar xf libusb-1.0.24.tar.bz2
RUN cd libusb-1.0.24 && \
    ./configure --disable-udev && \
    make -j && make install

RUN pip install -U pip && pip install --extra-index-url https://www.piwheels.org/simple/ --prefer-binary opencv-python

COPY . /depthai-python
RUN cd /depthai-python && python3 -m pip install .

```

At `~/depthai-python`, build and run the docker container
```code
sudo docker build -t depthai-python-cudagl -f ci/Dockerfile_CUDAGL .
sudo docker run --rm \
    --runtime=nvidia -ti\
    --privileged \
    -v /dev/bus/usb:/dev/bus/usb \
    --device-cgroup-rule='c 189:* rmw' \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    depthai-python-cudagl \
    python3 /depthai-python/examples/ColorCamera/rgb_preview.py

```