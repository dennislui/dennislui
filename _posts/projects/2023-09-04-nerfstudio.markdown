---
layout:     post
title:      "Nerfstudio Experimentation"
date:       2023-09-04 16:12:00 0800
image:      nerfstudio-2023.png
youtubeId1: 5oWARGekaog 
youtubeId2: 0CFCu76wsCQ 
youtubeId3: uPmlCNInAJE
categories: [projects,ai]
tag:        posts
---

[Nerfstudio](https://docs.nerf.studio/en/latest/#) was a breeze to setup and works with CUDA version >= 11.8. 

Opted for the Docker container which can be pulled down with the following command

```docker pull dromni/nerfstudio:0.3.3```

Once the 'poster' dataset is downloaded and training is started, it comes with a nice web visualizer that looks like the following:

{:.center}
{% include youtubePlayer.html id=page.youtubeId1 %}

\\
After training completes (at 29999 steps), the trained model can be reloaded. A camera path can be defined for video rendering.
The following videos shows the same camera path rendered using nerfacto (left) and instant-ngp (right) models.


{% include youtubePlayerSmall.html id=page.youtubeId3 %}
{% include youtubePlayerSmall.html id=page.youtubeId2 %}


References
==========

{:.paper}
<span>Nerfstudio: A Modular Framework for Neural Radiance Field Development</span>{:.papertitle}  
<span>M. Tancik, E. Weber, E. Ng, R. Li, B. Yi, J. Kerr, T. Wang, A. Kristoffersen, J. Austin, K. Salahi, A. Ahuja, D. McAllister and A. Kanazawa </span>{:.authors}  
<span>_ACM SIGGRAPH 2023 Conference Proceedings_, 2023</span>{:.journal} 
