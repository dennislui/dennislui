---
layout:     post
title:      "TSDF Volumetric Ray Casting"
date:       2020-09-06 09:37:00 0800
image:      tsdf-volumetric-raycasting-2020.png
youtubeId:  ERH8hJyk2MY
categories: [projects]
tag:        projects
---

Video shows the real time construction of a Truncated Signed Distance Function (TSDF) volume from depth images and then ray-casting it for visualization.

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}