---
layout:		post
title:		"Staircase detection using plane models"
date:		2012-10-01 15:32:14 0800
image: 		staircase-detection-2012-1.png
youtubeId:	7LZmL7cXCCU
categories:	[projects]
tag:		projects
---

The eBug is a low-cost and open robotics platform designed for undergraduate teaching and academic research in areas such as multimedia smart sensor networks, distributed control, mobile wireless communication algorithms and swarm robotics. The platform is easy to use, modular and extensible.  Please refer selected publication for more information.

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}

Selected Publication
====================

{:.paper}
<span>Plane-based detection of staircases using inverse depth</span>{:.papertitle}  
<span>T. J. J. Tang, W. L. D. Lui, and W. H. Li</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Wellington, New Zealand, 2012</span>{:.journal}
