---
layout:		post
title:		"Autonomous Robot Navigation: Appearance based Topological SLAM"
date:		2012-01-01 15:32:14 0800
image: 		autonomous-robot-navigation-2012-1.png
youtubeId1:	Y_rXRWD7eOI
youtubeId2: k6Qu98TrKQI
youtubeId3: bj2IJBzQO5o
youtubeId4: z077PZpPjnI
youtubeId5: 4u5cBLzqitY
categories:	[projects]
tag:		projects
---

The main focus of this research is to develop an autonomous robot capable of self-navigation in an unknown environment. The proposed system performs autonomous navigation primarily based on the following visually perceived information:

* **Range Estimation**: A novel variable single/multi baseline omnidirectional stereovision system with automatic baseline selection (processing offloaded to the GPU).  It also features a Bumblebee stereo camera for dynamic obstacle avoidance.

{:.center}
{% include youtubePlayer.html id=page.youtubeId3 %}

* **Motion Estimation**: A 3DoF visual odometry system combining distance travelled estimated by a ground plane optical flow tracking system, with bearing estimated by the panoramic visual compass system.

{:.center}
{% include youtubePlayer.html id=page.youtubeId1 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId2 %}

* **Place Recognition**: An appearance-based place recognition system using image signatures created from Haar decomposed omnidirectional images for loop closure detection.

These modules are integrated into the mobile robot’s navigation and mapping system which balances its effort amongst loop closing and exploration, decides its next course of action, performs topological map building and path planning, and executing the selected path. For more details, please refer to the selected publications

{:.center}
{% include youtubePlayer.html id=page.youtubeId4 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId5 %}


Selected Publications
=====================

{:.paper}
<span>An Omnidirectional Vision System for Outdoor Mobile Robots</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_SIMPAR Workshop on Omnidirectional Robot Vision_, Venice, Italy, 2008, pp. 273-284</span>{:.journal}  

{:.paper}
<span>A Pure Vision-based Approach to Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Intelligent Robots and Systems_, Taipei, Taiwan, 2010, pp. 3784-3791</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/IROS.2010.5651115](http://dx.doi.org/10.1109/IROS.2010.5651115)</span>{:.doi}

{:.paper}
<span>An Active Visual Loop Closure Detection and Validation System for Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Brisbane, Australia, 2010</span>{:.journal}  

{:.paper}
<span>Eye-Full Tower: A GPU-based variable multibaseline omnidirectional stereovision system with automatic baseline selection for outdoor mobile robot navigation</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_Robotics and Autonomous Systems_, vol. 58, iss. 6, pp. 747-761, 2010</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1016/j.robot.2010.02.007](http://dx.doi.org/10.1016/j.robot.2010.02.007)</span>{:.doi}

{:.paper}
<span>Autonomous Robot Navigation: Appearance based Topological SLAM</span>{:.papertitle}  
<span>W. L. D. Lui</span>{:.authors}  
<span>PhD Thesis, Monash University, Australia, 2011</span>{:.journal}  
<span>**Link:** [http://arrow.monash.edu.au/hdl/1959.1/473850](http://arrow.monash.edu.au/hdl/1959.1/473850)</span>{:.doi}

{:.paper}
<span>A Pure Vision-based Topological SLAM System</span>{:.papertitle}  
<span>W. L. D. Lui and R. Jarvis</span>{:.authors}  
<span>_The International Journal of Robotics Research_, vol. 31, iss. 4, pp. 403-428, 2012</span>{:.journal}  
<span>**Link:** [http://ijr.sagepub.com/content/31/4/403](http://ijr.sagepub.com/content/31/4/403)</span>{:.doi}
