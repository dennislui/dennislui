---
layout:		post
title:		"Image Processing for Bionic Vision"
date:		2013-04-06 15:32:14 0800
image: 		bionic-eye-1.png
youtubeId:	iK5ddJqNuxY
youtubeId2:	J30uYYkDApY
categories:	[projects]
tag:		projects
---

Visual prostheses such as retinal implants provide bionic vision that is limited in spatial and intensity resolution. This limitation is a fundamental challenge of bionic vision as it severely truncates salient visual information. We propose to address this challenge by performing real time transformations of visual and non-visual sensor data into symbolic representations that are then rendered as low resolution vision; A concept we call Transformative Reality.

For more information, please refer to selected publications below. This work was supported by the Australian Research Council Special Research Initiative in Bionic Vision and Sciences (SRI 1000006).

{:.center}
{% include youtubePlayer.html id=page.youtubeId2 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}

![bionicEye-2]({{"/images/bionic-eye-2.png" | prepend: site.baseurl }}){:width="360px"}
![bionicEye-3]({{"/images/bionic-eye-3.png" | prepend: site.baseurl }}){:width="358px"}

Selected Publications
=====================

{:.paper}
<span>Transformative Reality: Augmented reality for visual prostheses</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the IEEE International Symposium on Mixed and Augmented Reality_, Basel, Switzerland, 2011, pp. 253-254</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ISMAR.2011.6092402](http://dx.doi.org/10.1109/ISMAR.2011.6092402)</span>{:.doi} 

{:.paper}
<span>Going beyond vision to improve bionic vision</span>{:.papertitle}  
<span>W. H. Li, T. J. J. Tang, and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Image Processing_, Melbourne, Australia, 2013, pp. 1555-1558</span>{:.journal}  
<span>**DOI:** [hhttp://dx.doi.org/10.1109/ICIP.2013.6738320](http://dx.doi.org/10.1109/ICIP.2013.6738320)</span>{:.doi} 

{:.paper}
<span>Transformative Reality: Improving bionic vision with robotic sensing</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the International Conference of the IEEE Engineering in Medicine and Biology Society_, CA, USA, 2012, pp. 304-307</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/EMBC.2012.6345929](http://dx.doi.org/10.1109/EMBC.2012.6345929)</span>{:.doi} 

{:.paper}
<span>System and Method for Processing Sensor Data for the Visually Impaired</span>{:.papertitle}  
<span>W. L. D. Lui, D. Browne, L. Kleeman, T. Drummond, and W. H. Li</span>{:.authors}  
<span>iss. PCT/AU2012/001006, 2012</span>{:.journal}