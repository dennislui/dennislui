---
layout:     post
title:      "Quick benchmark of Inverse Depth ICP"
date:       2021-02-28 16:50:00 0800
image:      icp-plant-dataset.png
youtubeId: xT0-Xa6cZHE
categories: [projects,robotics]
tag:        posts
---

Dug up some old code (previous work on inverse depth ICP) and compared it against the [TUM RGBD dataset](https://vision.in.tum.de/data/datasets/rgbd-dataset), 
specfically on the [plant dataset](https://vision.in.tum.de/data/datasets/rgbd-dataset/download#).

{: .center}
![icp-plant-dataset]({{"/images/icp-plant-dataset.png" | prepend: site.baseurl }}){:width="640px"}

Using the scripts provided with the datasets, I have calculated the absolute trajectory error and visualized it in the following plots.

Absolute Translational Errors (Inverse Depth ICP) = 0.100429 m  
Absolute Translational Errors (RGBD-SLAM) = 0.061407 m

The plot on the left shows the trajectory estimated using inverse depth ICP, whereas the plot on the right shows the trajectory estimated by RGBD-SLAM. This dataset is 41.53 seconds and the total trajectory is 14.795m.

{: .center}
![icp-plant-dataset-ate]({{"/images/icp-plant-dataset-ate.png" | prepend: site.baseurl }}){:width="320px"}
![rgbdslam-plant-dataset-ate]({{"/images/rgbdslam-plant-dataset-ate.png" | prepend: site.baseurl }}){:width="320px"}

Not bad given that the inverse depth ICP method is only using depth information and the absolute trajectory is based on estimated interframe motion. Last but not least, you can see a replay of the estimated trajectory in the video below.

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}