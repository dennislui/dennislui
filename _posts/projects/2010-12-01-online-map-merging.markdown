---
layout:		post
title:		"Online Map Merging"
date:		2010-10-01 15:32:14 0800
image: 		online-map-merging-2010-1.png
youtubeId:	dQemNJX3kAY
categories:	[projects]
tag:		projects
---

This project is the outcome of the collaborative effort between Fredy Tungadi and myself. This system aims to ensure the lifelong operation of the mobile robot by equipping it with map merging capabilities such that the robot can be switched on and off at different times and days and at different locations in a fully or partially static environment. It is equipped with two Hokuyo laser rangefinders and an omnidirectional vision system and performs map merging by using laser scan matching and a probabilistic place recognition system based on omnidirectional images.

For more information, please refer selected publication.

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}

Selected Publication
====================

{:.paper}
<span>Robust Online Map Merging System using Laser Scan Matching and Omnidirectional Vision</span>{:.papertitle}  
<span>F. Tungadi, W. L. D. Lui, L. Kleeman, and R. Jarvis</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Intelligent Robots and Systems_, Taipei, Taiwan, 2010, pp. 7-14</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/IROS.2010.5654446](http://dx.doi.org/10.1109/IROS.2010.5654446)</span>{:.doi}
