---
layout:		post
title:		"6DoF Egomotion Estimation using Depth Images"
date:		2012-09-29 15:32:14 0800
image: 		6dof-egomotion-2014-1.png
youtubeId1:	qZbsuwzkZp4
youtubeId2:	4ae_kHeygIw
categories:	[projects]
tag:		projects
---

Egomotion estimation is the problem of estimating a moving camera’s pose relative to a base coordinate frame, such as the camera’s initial position, from a sequence of images. This is important to a wide range of applications, including mobile robotics and augmented reality. Instead of using Euclidean coordinates, our solutions use inverse depth coordinates which demonstrate better noise characteristics, similar to the raw sensor data. The performance of frame-to-frame egomotion estimation based on the detection of planes and the Iterative Closest Point (ICP) algorithm is described in selected publications.

{:.center}
{% include youtubePlayer.html id=page.youtubeId1 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId2 %}

Selected Publications
=====================

{:.paper}
<span>A lightweight approach to 6-DOF plane-based egomotion estimation using inverse depth</span>{:.papertitle}  
<span>T. J. J. Tang, W. L. D. Lui, and W. H. Li</span>{:.authors}  
<span>_Australasian Conference on Robotics and Automation_, Melbourne, Australia, 2011</span>{:.journal}

{:.paper}
<span>Robust egomotion estimation using ICP in inverse depth coordinates</span>{:.papertitle}  
<span>W. L. D. Lui, T. J. J. Tang, and W. H. Li</span>{:.authors}  
<span>_Proceedings of the IEEE International Conference on Robotics and Automation_, MN, USA, 2012, pp. 1671-1678</span>{:.journal}  
<span>**DOI:** [http://dx.doi.org/10.1109/ICRA.2012.6225354](http://dx.doi.org/10.1109/ICRA.2012.6225354)</span>{:.doi} 
