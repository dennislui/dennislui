---
layout:		post
title:		"Neural Q-Learning Algorithms using Webots"
date:		2009-12-01 15:32:14 0800
image: 		q-learning-2009-1.png
youtubeId1:	uxaAOD2IRxU
youtubeId2:	6-UbJECEDBY
youtubeId3:	W1NPZybT3k0
youtubeId4:	-cQ8oC1rg3o
categories:	[projects]
tag:		projects
---

This work demonstrates the capability of a Khepera II robot learning specific behaviours (e.g. obstacle avoidance and wall following) by utilizing Neural Q-Learning controllers based on both IR and vision sensors. A highly flexible simulator is developed using Webots for testing the developed controllers before they are validated on the actual robot.  For more information, please refer to selected publications

Obstacle avoidance behavior on real and simulated robot

{:.center}
{% include youtubePlayer.html id=page.youtubeId1 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId2 %}

Wall following behavior on real and simulated robot

{:.center}
{% include youtubePlayer.html id=page.youtubeId3 %}

{:.center}
{% include youtubePlayer.html id=page.youtubeId4 %}

Selected Publications
=====================

{:.paper}
<span>Design and Implementation of the Khepera II using Webots Software</span>{:.papertitle}  
<span>W. L. D. Lui</span>{:.authors}  
<span>_Monash University_, 2006</span>{:.journal}  
<span>Monash Undergraduate Honours Thesis</span>{:.comment}  

{:.paper}
<span>Design and Implementation of a Simulated Robot using Webots Software</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Control, Instrumentation and Mechatronics Engineering_, Johor Bahru, Malaysia, 2008, pp. 850-856</span>{:.journal}  

{:.paper}
<span>Application of Neural Q-Learning on the Khepera-II via Webots Software</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the International Conference on Fascinating Advancement in Mechanical Engineering_, Tamil Nadu, India, 2008</span>{:.journal}  

{:.paper}
<span>Utilization of Webots and the Khepera II as a Platform for Neural Q-Learning Controllers</span>{:.papertitle}  
<span>V. Ganapathy, C. Y. Soh, and W. L. D. Lui</span>{:.authors}  
<span>_Proceedings of the IEEE Symposium on Industrial Electronics and Applications_, Kuala Lumpur, Malaysia, 2009, pp. 783-788</span>{:.journal}  
<span>**Link:** [http://dx.doi.org/10.1109/ISIEA.2009.5356361](http://dx.doi.org/10.1109/ISIEA.2009.5356361)</span>{:.doi}
