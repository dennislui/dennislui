---
layout:		post
title:		"Automatic license plate recognition"
date:		2008-12-01 15:32:14 0800
image: 		alpr-2008-1.png
youtubeId:	3GJWvsUIiyk
categories:	[projects]
tag:		projects
---

The proposed automatic license plate recognition system is developed specifically for vehicles in Malaysia. It uses a novel license plate segmentation algorithm based on a combination of Hough Transform and morphological operations. Instead of locating the rectangular border of the license plate in the image, our approach is primarily focused in detecting the location of the characters on the license plate. This method was thoroughly tested and have automatically and correctly recognize 95% of license plates in a total of 589 images captured in complex outdoor scenes.  For more information, please refer to the selected publication

{:.center}
{% include youtubePlayer.html id=page.youtubeId %}

Selected Publication
====================

{:.paper}
<span>A Malaysian icense late localization and recognition system</span>{:.papertitle}  
<span>V. Ganapathy and W. L. D. Lui</span>{:.authors}  
<span>_Journal of Systemics, Cybernetics and Informatics_, vol. 6, iss. 1, 2008</span>{:.journal}  
