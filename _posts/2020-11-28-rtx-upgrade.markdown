---
layout:     post
title:      "RTX Upgrade"
date:       2020-11-28 17:00:00 -0700
image:      rtx-upgrade-2020-1.jpg
categories: [robotics]
tag:        posts
---

{:.center}
![rtx-upgrade-2020-1]({{"/images/rtx-upgrade-2020-1.jpg" | prepend: site.baseurl }}){:width="320px"} ![rtx-upgrade-2020-2]({{"/images/rtx-upgrade-2020-2.jpg" | prepend: site.baseurl }}){:width="320px"}

{:.center}
![rtx-upgrade-2020-3]({{"/images/rtx-upgrade-2020-3.jpg" | prepend: site.baseurl }}){:width="320px"}

Time to upgrade from my 1080ti to a RTX 3070! Can't wait to see RTX being used in Cyberpunk 2077!
