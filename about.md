---
layout: page
title: About
permalink: /about/
nav: true
---

{: .center}
![Image]({{"/images/dennis-avatar.png" | prepend: site.baseurl }} "Image_circle"){:.img-avatar}

{: style="text-align: justify"}
I completed my PhD in mobile robotics and computer vision from Monash University, Australia in 2011. Upon graduation, I joined the [Monash Vision Group](https://www.monash.edu/bioniceye) as a post-doctoral researcher to work on a direct to brain bionic eye system. In 2013, I took up a position at [ANCA](https://machines.anca.com/ToolRoom), a world leading manufacturer of tool cutter grinders, as a software engineer within the software R&D team to create and enable new grinding operations that produces highly precise and specialized tool cutters. In December 2014, I joined [Blackmagic Design](https://www.blackmagicdesign.com/products#professional-cameras) as a senior software engineer to work on camera calibration for a range of professional video cameras. Towards the end of 2016, I have relocated to California to take up the senior solutions architect role for autonomous driving at [Nvidia](https://www.nvidia.com/en-us/self-driving-cars/). Today, I manage and lead a team of cross functional engineers and architects, working closely with partners to bring autonomous machines into the market.

{: style="text-align: justify"}
My research interests include computer vision, assistive technologies, robotics and artificial intelligence – specifically targeting real-time applications. Follow this [link]({{ "/download/resume.pdf" | prepend: site.baseurl }}) to download a copy of my resume for your reference.
